const webpack = require('webpack');
const path = require('path');

const autoprefixer = require('autoprefixer');
const postcssFlexbugsFixes = require('postcss-flexbugs-fixes');

const HtmlWebpackPlugin = require('html-webpack-plugin');
const ExtractTextWebpackPlugin = require('extract-text-webpack-plugin');

const NODE_PRODUCTION = process.env.NODE_ENV === 'production';
const NODE_DEVELOPMENT = process.env.NODE_ENV === 'development';
const SRC_DIR = path.resolve(__dirname, './src');

const extractSass = new ExtractTextWebpackPlugin({
  filename: "[name].[contenthash].css",
  disable: process.env.NODE_ENV === "development"
});

const BROWSERLIST = [
  'last 2 Chrome versions',
  'last 2 Firefox versions',
  'last 2 Opera versions',
  'last 2 Edge versions',
  'last 2 Safari versions'
];

const getPlugins = () => {
  const plugins = [
    new HtmlWebpackPlugin({
      template: path.resolve(__dirname, './static/index.html')
    }),
    extractSass
  ];

  if (NODE_DEVELOPMENT) {
    plugins.push(new webpack.HotModuleReplacementPlugin());
  }

  if (NODE_PRODUCTION) {
    plugins.push(
      new webpack.optimize.UglifyJsPlugin({
        compress: {
          warnings: false
        },
        sourceMap: true
      })
    );
  }

  return plugins;
};

module.exports = {
  entry: {
    app: './src/index.js'
  },

  output: {
    filename: 'bundle.js',
    path: path.resolve(__dirname, 'build'),
    publicPath: '/'
  },

  module: {
    rules: [
      {
        test: /\.js$/,
        enforce: 'pre',
        include: SRC_DIR,
        loader: 'eslint-loader',
        options: {
          failOnError: true
        }
      },
      {
        test: /\.svg$/,
        include: SRC_DIR,
        use: [
          {
            loader: 'url-loader',
            options: {
              limit: 10000
            }
          },
          {
            loader: 'svgo-loader',
            options: {
              plugins: [{ removeUselessDefs: true }]
            }
          }
        ]
      },
      {
        test: /\.woff|.woff2$/,
        include: SRC_DIR,
        loader: 'file-loader',
        options: {
          limit: 10000,
          name: 'fonts/[name].[hash:8].[ext]'
        }
      },
      // здесь у меня ничего не получилось
      {
        test: /\.(jpe?g|png|gif|ico)$/i,
        include: SRC_DIR,
        use: [
          {
            loader: 'file-loader',
            options: {
              name: 'images/[name].[ext]'
            }
          }
        ]
      },
      {
        test: /\.scss$/,
        use: extractSass.extract({
          fallback: 'style-loader',
          use: [
            {
              loader: 'css-loader',
            },
            {
              loader: 'postcss-loader',
              options: {
                plugins: [
                  postcssFlexbugsFixes,
                  autoprefixer({
                    browsers: BROWSERLIST,
                    flexbox: 'no-2009'
                  })
                ]
              }
            },
            {
              loader: 'sass-loader'
            }
          ]
        })
      },
      {
        test: /\.js$/,
        include: SRC_DIR,
        loader: 'babel-loader',
        options: {
          presets: [
            [
              'env',
              {
                targets: {
                  browsers: BROWSERLIST,
                  uglify: true
                },
                modules: false,
                loose: true,
                useBuiltIns: true,
                debug: true
              }
            ],
            'react'
          ],
          plugins: [
            'transform-class-properties',
            'transform-object-rest-spread'
          ],
          cacheDirectory: true
        }
      }
    ]
  },

  plugins: getPlugins(),
  devtool: NODE_PRODUCTION ? 'source-map' : 'cheap-module-source-map',
  performance: {
    hints: NODE_PRODUCTION && 'warning'
  },
  devServer: {
    host: '127.0.0.1',
    port: 8090,
    disableHostCheck: true,
    historyApiFallback: true
  }
};
