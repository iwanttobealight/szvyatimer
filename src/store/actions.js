import * as actionTypes from './actionTypes';
import axios from '../axios-date';

export const setNumOfDays = (lastActiveDay) => {
  return {
    type: actionTypes.SET_NUM_OF_DAYS,
    lastActiveDay: lastActiveDay
  }
}

export const onConfirmButtonClick = (lastActiveDay) => {
  return {
    type: actionTypes.ON_CONFIRM_BUTTON_CLICK,
    lastActiveDay: lastActiveDay
  }
}

export const onCancelButtonClick = () => {
  return { type: actionTypes.ON_CANCEL_BUTTON_CLICK }
}

export const onResetButtonClick = () => {
  return { type: actionTypes.ON_RESET_BUTTON_CLICK }
}

export const errorHandler = (message) => {
  return {
    type: actionTypes.ERROR_HANDLER,
    message: message
  }
}

export const getDate = () => {
  return dispatch => {
    axios
      .get('https://szvyatimer-database.firebaseio.com/active-days/last-active-day.json')
      .then(response => {
        dispatch(setNumOfDays(response.data));
      })
      .catch(error => {
        dispatch(errorHandler(error.message));
      });
  };
}

const generateCurrentDate = () => { return { 'last-active-day': new Date().toDateString() } }

export const resetDate = () => {
  return dispatch => {
    const lastActiveDay = generateCurrentDate()['last-active-day'];
    dispatch(onConfirmButtonClick(lastActiveDay));
    axios
      .put("/active-days.json", generateCurrentDate())
      .catch(error => {
        dispatch(errorHandler(error.message));
      })
  };
}
