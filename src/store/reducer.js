import * as actionTypes from './actionTypes';

import { updateObject } from './utils';

const initialState = {
	counter: '...',
	visiblePopup: false,
	visibleError: false,
	errorMessage: ''
}

const countRange = (lastActiveDay) => {
	const currentDate = new Date().toDateString();
	const distinction = Math.abs(Math.floor((Date.parse(lastActiveDay) - Date.parse(currentDate)) / 86400000));
	return { lastActiveDay: lastActiveDay, distinction: distinction }
}

const setNumOfDays = (state, action) => {
	return updateObject(state, {
		counter: countRange(action.lastActiveDay).distinction,
		lastActiveDay: countRange(action.lastActiveDay).lastActiveDay
	})
}

const onConfirmButtonClick = (state, action) => {
	return updateObject(state, {
		visiblePopup: false,
		counter: countRange(action.lastActiveDay).distinction,
		lastActiveDay: countRange(action.lastActiveDay).lastActiveDay
	})
}

const onCancelButtonClick = (state) => {
	return updateObject(state, { visiblePopup: false });
}

const onResetButtonClick = (state) => {
	return updateObject(state, { visiblePopup: true });
}

const errorHandler = (state, action) => {
	return updateObject(state, { visibleError: true, errorMessage: action.message.toLowerCase() })
}

const reducer = (state = initialState, action) => {
	switch (action.type) {
		case actionTypes.SET_NUM_OF_DAYS:
			return setNumOfDays(state, action)
		case actionTypes.ON_CONFIRM_BUTTON_CLICK:
			return onConfirmButtonClick(state, action)
		case actionTypes.ON_CANCEL_BUTTON_CLICK:
			return onCancelButtonClick(state)
		case actionTypes.ON_RESET_BUTTON_CLICK:
			return onResetButtonClick(state)
		case actionTypes.ERROR_HANDLER:
			return errorHandler(state, action)
		default:
			return state
	}
}

export default reducer;