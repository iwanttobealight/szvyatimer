import React from 'react';
import {render} from 'react-dom';
import {BrowserRouter} from 'react-router-dom';
import {createStore, applyMiddleware, compose} from 'redux';
import {Provider} from 'react-redux';
import thunk from 'redux-thunk';

// если я импортирую App линтер ругается втф линтер
import MyApp from './MyApp';
import reducer from './store/reducer';

import './styles.scss';

const composeEnhancers = window.__REDUX__DEVTOOLS__EXTENSION__COMPOSE || compose;

const store = createStore(reducer, composeEnhancers(applyMiddleware(thunk)));

const app = (
    <Provider store={store}>
        <BrowserRouter>
            <MyApp/>
        </BrowserRouter>
    </Provider>
)

render(app, document.getElementById('app'));