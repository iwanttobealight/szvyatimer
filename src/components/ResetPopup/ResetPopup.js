import React from 'react';
import PropTypes from 'prop-types';

import ModalOverlay from '../ModalOverlay/ModalOverlay';
import Button from '../Button/Button';

import './ResetPopup.scss';

const ResetPopup = (props) => (
  <React.Fragment>
    <ModalOverlay visible={props.visible} />
    <div
      className="reset-popup"
      style={{
        transfrom: props.visible ? 'translateY(0)' : 'translateY(-100vh)',
        opacity: props.visible ? '1' : '0',
        zIndex: props.visible ? '2' : '-1'
      }}>
      <p className="reset-title">Уверен?</p>
      <div className="reset-buttons">
        <Button label='да' clicked={props.onConfirmButtonClick} />
        <Button label='нет' clicked={props.onCancelButtonClick} />
      </div>
    </div>
  </React.Fragment>
)


ResetPopup.propTypes = {
  visible: PropTypes.bool,
  onConfirmButtonClick: PropTypes.func.isRequired,
  onCancelButtonClick: PropTypes.func.isRequired
}

ResetPopup.defaultProps = {
  visible: true
}

export default ResetPopup;