import React from 'react';
import PropTypes from 'prop-types';

import './Input.scss';

const Input = (props) => (
  <label className="input__item">
    {props.label}:
    <input
      className={`input ${props.inputClassName}${props.invalid && props.touched ? 'input__invalid' : ''}`}
      value={props.value}
      placeholder={props.placeholder}
      type={props.type}
      onChange={props.changed}
    />
  </label>
);

Input.propTypes = {
  value: PropTypes.string.isRequired,
  placeholder: PropTypes.string.isRequired,
  changed: PropTypes.func.isRequired,
  inputClassName: PropTypes.string,
  invalid: PropTypes.bool,
  touched: PropTypes.bool,
  label: PropTypes.string,
  type: PropTypes.string
}

Input.defaultProps = {
  inputClassName: '',
  label: 'Ваши данные',
  type: 'text',
  invalid: false,
  touched: false
}

export default Input;