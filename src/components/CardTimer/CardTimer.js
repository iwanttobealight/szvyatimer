import React from 'react';
import PropTypes from 'prop-types';

import './CardTimer.scss';

const CardTimer = (props) => (
  <React.Fragment>
    <p className="text">не покупал игры в стиме уже<span className="counter">&nbsp;{props.counter}&nbsp;</span>дней</p>
  </React.Fragment>
)

CardTimer.propTypes = {
  counter: PropTypes.PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number]).isRequired
}

export default CardTimer;
