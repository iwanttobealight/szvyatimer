import React from 'react';
import PropTypes from 'prop-types';

import './ErrorPopup.scss';

const ErrorPopup = (props) => (
  props.visible ? <div className='error'>{`you have an error: ${props.errorMessage}`}</div> : null
)

ErrorPopup.propTypes = {
  errorMessage: PropTypes.string,
  visible: PropTypes.bool.isRequired
}

ErrorPopup.defaultProps = {
  errorMessage: 'unhandled error (sad story)'
}

export default ErrorPopup;