import React from 'react';
import PropTypes from 'prop-types';

import './ModalOverlay.scss';

const ModalOverlay = (props) => (
  props.visible ? <div className="modal-overlay"/> : null
);

ModalOverlay.propTypes = {
  visible: PropTypes.bool.isRequired
}


export default ModalOverlay;