import React from 'react';
import PropTypes from 'prop-types';

import './Button.scss';

const Button = (props) => (
  <button
  className={`button ${props.buttonClassName}`}
  onClick={props.clicked}
  type={props.type}
  disabled={props.disabled}
  >{props.label}
  </button>
)

Button.propTypes = {
  clicked: PropTypes.func.isRequired,
  buttonClassName: PropTypes.string,
  disabled: PropTypes.bool,
  label: PropTypes.string,
  type: PropTypes.string
}

Button.defaultProps = {
  label: 'click me',
  type: 'button',
  buttonClassName: '',
  disabled: false
}

export default Button;