import axios from 'axios';

const instance = axios.create({
  baseURL: 'https://szvyatimer-database.firebaseio.com/'
});

export default instance;