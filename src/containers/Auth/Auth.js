import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';

import Button from '../../components/Button/Button';
import NavPanel from '../NavPanel/NavPanel';
import Input from '../../components/Input/Input';

import './Auth.scss';

export class Auth extends Component {

    state = {
        inputs: {
            email: {
                value: '1@1.s',
                type: 'email',
                placeholder: 'szvyat@pirozhoc.com',
                label: 'email',
                validation: {
                    required: true,
                    isEmail: true
                } ,
                valid: false,
                touched: false
            },
            password: {
                value: '12345678',
                type: 'password',
                placeholder: '1234567',
                label: 'пароль',
                validation: {
                    required: true,
                    minLength: 6
                },
                valid: false,
                touched: false
            }
        },
        formIsValid: false
    }

    clicked = () => {
        console.log('send msg: "ifff pidor"');
    }

    checkValidity = (value, rules) => {
        let isValid = true;
        if (!rules) {
            return true;
        }

        if (rules.required) {
            isValid = value.trim() !== '' && isValid;
        }

        if (rules.minLength) {
            isValid = value.length >= rules.minLength && isValid
        }

        if (rules.isEmail) {
            const pattern = /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/;
            isValid = pattern.test(value) && isValid;
        }

        return isValid;
    }

    inputChangeHandler = (e, inputName) => {
        const updatedInputs = {
            ...this.state.inputs,
            [inputName]: {
                ...this.state.inputs[inputName],
                value: e.target.value,
                valid: this.checkValidity(e.target.value, this.state.inputs[inputName].validation),
                touched: true
            }
        }

        let formIsValid = true;
        for (let inputIdentifier in updatedInputs) {
            formIsValid = updatedInputs[inputIdentifier].valid && formIsValid;
        }
        this.setState({ inputs: updatedInputs, formIsValid: formIsValid });
    }

    render() {

        const inputs = [];
        for (let i in this.state.inputs) {
            inputs.push({ id: i, config: this.state.inputs[i] });
        }

        return (
            <div className="app" method="/post">
                <NavPanel />
                <form className="auth">
                    {inputs.map(inputElement => (<Input
                        key={inputElement.id}
                        type={inputElement.config.type}
                        value={inputElement.config.value}
                        placeholder={inputElement.config.placeholder}
                        label={inputElement.config.label}
                        invalid={!inputElement.config.valid}
                        touched={inputElement.config.touched}
                        changed={(e) => this.inputChangeHandler(e, inputElement.id)}
                    />))}
                    <Button 
                    label='войти'
                    buttonClassName='button__signin'
                    clicked={this.clicked}
                    disabled={!this.state.formIsValid} />
                    <Button 
                    label='зарегистрироваться'
                    buttonClassName='button__signup'
                    clicked={this.clicked}
                    disabled={!this.state.formIsValid} />
                </form>
            </div>
        )
    }
}


export default withRouter(Auth);

