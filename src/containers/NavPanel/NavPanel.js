import React, { Component } from 'react';
import { withRouter, NavLink } from 'react-router-dom';

import './NavPanel.scss';

export class NavPanel extends Component {

    render() {
        return (
            <div className="nav-panel">
                <NavLink
                    to="/auth"
                    className="nav-link"
                    activeClassName="nav-link__active"
                    exact>Auth</NavLink>
                <NavLink
                    to="/"
                    className="nav-link"
                    activeClassName="nav-link__active"
                    exact>App</NavLink>
            </div>
        )
    }
}


export default withRouter(NavPanel);