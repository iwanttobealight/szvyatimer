import React, { Component } from 'react';
import { Route, withRouter, Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import * as actionCreators from '../../store/index';

import Button from '../../components/Button/Button';
import CardTimer from '../../components/CardTimer/CardTimer';
import ResetPopup from '../../components/ResetPopup/ResetPopup';
import ErrorPopup from '../../components/ErrorPopup/ErrorPopup';
import NavPanel from '../NavPanel/NavPanel';

import './App.scss'

export class App extends Component {

  componentDidMount() {
    this.props.getDate();
  };

  render() {

    return (
      <div className="app">
        <NavPanel />
        <CardTimer counter={this.props.counter} />
        <Button
          label='сбросить'
          clicked={this.props.onResetButtonClick}
          buttonClassName='button--reset' />
        <ResetPopup
          visible={this.props.visiblePopup}
          onConfirmButtonClick={this.props.resetDate}
          onCancelButtonClick={this.props.onCancelButtonClick} />
        <ErrorPopup
          visible={this.props.visibleError}
          errorMessage={this.props.errorMessage} />
      </div>
    )
  }
}

App.propTypes = {
  counter: PropTypes.PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired,
  getDate: PropTypes.func.isRequired,
  resetDate: PropTypes.func.isRequired,
  onConfirmButtonClick: PropTypes.func.isRequired,
  onCancelButtonClick: PropTypes.func.isRequired,
  onResetButtonClick: PropTypes.func.isRequired,
  visiblePopup: PropTypes.bool,
  errorMessage: PropTypes.string.isRequired
}

App.defaultProps = {
  visiblePopup: false
}

const mapStateToProps = state => {
  return {
    counter: state.counter,
    lastActiveDay: state.lastActiveDay,
    visiblePopup: state.visiblePopup,
    visibleError: state.visibleError,
    errorMessage: state.errorMessage
  }
}

const mapDispatchToProps = dispatch => {
  return {
    getDate: () => dispatch(actionCreators.getDate()),
    resetDate: () => dispatch(actionCreators.resetDate()),
    onConfirmButtonClick: () => dispatch(actionCreators.onConfirmButtonClick()),
    onCancelButtonClick: () => dispatch(actionCreators.onCancelButtonClick()),
    onResetButtonClick: () => dispatch(actionCreators.onResetButtonClick())
  }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(App));