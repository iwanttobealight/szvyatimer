import React, { Component } from 'react';
import { Route, withRouter, Redirect } from 'react-router-dom';

import MainApp from './containers/App/App';
import Authorization from './containers/Auth/Auth';

class MyApp extends Component {
    render() {
        return (
            <React.Fragment>
                <Route path="/auth" component={Authorization} />
                <Route path="/" exact component={MainApp} />
                <Redirect to="/auth" />
            </React.Fragment>
        )
    }
}

export default withRouter(MyApp);